#include <stdio.h>
#include <stdlib.h>

/*
 * Funções de entrada e saída:
 * 	Entrada:
 * 		scanf( const char * restrict fmt, ... )
 *
 * 	Saída:
 * 		puts( const char * string );
 * 		printf( const char * restrict fmt, ... );
 *
 * Para mais infomações, utiliza o comando man do unix
 * 
 * 	man 3 puts
 * 	man 3 scanf
 * 	man 3 printf
 *
 * A string com a formatação, valida para printf e scanf,  pode conter 
 * os seguintes campos:
 * 	%%	 - Imprime o caracter %
 * 	%d	 - Valor inteiro com sinal
 * 	%u  - Valor imteiro sem sinal
 * 	%f  - Valores float no formato X.YYYYYY
 * 	%e  - Valores float no formato X.YYYYYYe+00
 *		%s  - String
 *		%c  - Um único caracter
 *
 * Caracteres especiais, preferencialmente utulizados em printf
 *		\a  - Beep ( não funciona em qualquer situação )
 *		\n  - New linw ( pula linha )
 *		\t  - Horizontal tan ( tabulação horizontal )
 *		\b  - Backspace ( retorna um espaço )
 *		\r  - Carrie Return ( retorno de carro )
 *		\f  - Form feed ( avança uma página )
 *		\0  - Char null ( caracter nulo \ZERO )
 *		\\  - Char \
 *		\"  - Char "
 *		\'  - Char '
 *
 */

int main(void);

int
main(void)
{
		  int i;
		  float f;
		  double d;

		  printf("Esta e uma string constante\n");
		  puts("Esta e uma outra string constante");
		  puts("Digite um valor inteiro");
		  scanf("%d", &i);
		  puts("Digite um valor de ponto flutuante");
		  scanf("%f", &f);
		  puts("Digite um outro valor de ponto flutuante");
		  scanf("%lf", &d);
		  printf("Voce digitou inteiro (%%d) : %d :\n",i);
		  printf("               Float (%%f) : %f :\n", f);
		  printf("               Float (%%e) : %e :\n", f);
		  printf("              Double (%%lf): %lf :\n", d);
		  return 0;
}
