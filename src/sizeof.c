#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int main(void);


int
main(void)
{
/*
 * Tipos padrão em C
 * 
 * 	inteiro com sinal: int;
 * 	inteiro sem sinal: unsigned int
 * 	Ponto flutuante: float, double
 * 	modificadores: short, long, * (ponteiro)
 */
	int a;
	char b;
	short int s;
	long int c;
	long long int d;
	float e;
	double f;
	long double g;
	uintmax_t h;
	uintmax_t *ph;
	int *pa;

	printf("\n\
	Tipo\t\tTamanho (bytes) \n\
	===============================\n\
	char\t\t%lu\n\
	short int\t%lu\n\
	int\t\t%lu\n\
	long int\t%lu\n\
	long long int\t%lu\n\
	float\t\t%lu\n\
	double\t\t%lu\n\
	long double\t%lu\n\
	uintmax_t\t%lu\n\
	uintmax_t *\t%lu\n\
	int *\t\t%lu\n",
		sizeof(b),sizeof(s),sizeof(a),
		sizeof(c),sizeof(d),sizeof(e),
		sizeof(f),sizeof(g),sizeof(h),
		sizeof(ph),	sizeof(pa));

	return 0;
}
