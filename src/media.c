#include <stdio.h>
#include <stdlib.h>
/*
 * Exercicio:
 * Faça os algoritimos descritivo e estruturado para calcular a média de 2
 * valores inteiros e mostrá-los na tela
 *
 * Descritivo:
 * Solicitar ao usuário 2 valores inteiros
 * Realizar a soma desses valores
 * Calcular a média
 * Mostrar na tela o resultado
 *
 * Estruturado:
 * 1. Inicio
 * 2. Declarar variáveis
 * 	2.1 media, soma, v1, v2
 * 3. Mostrar "Digite dois valores inteiros"
 * 4. Leia v1, v2
 * 5. Calcule soma <- v1 + v2
 * 6. Calcule media <- soma / 2
 * 6. Mostrar "Resultado da media (", v1, " + ", v2, ")/2 = ", soma
 * 7. Fim ( Término )
 */

int main(void);

int
main(void)
{
		  int soma, v1, v2;
		  float media;
		  puts("Digite 2 valores inteiros");
		  scanf("%d %d", &v1, &v2);
		  soma = v1 + v2;
		  media = soma / 2.0;
		  printf("Resultado da media de ( %d + %d ) = %f\n", v1, v2, media);

		  return 0;
}
