#include <stdio.h>
#include <stdlib.h>

#define ARRAY_LEN		0xFFFFF
int *set_array(int value, int **p);
int main(void);


int *
set_array(int value, int **p)
{
		  int c;
		  if ((*p = (int *)(malloc(sizeof(int) * value))) == (int *)(NULL))
					 printf("Error allocation\n");
		  return *p;
}


int
main(void)
{
		  int c;
		  int *r;

		  if (set_array( ARRAY_LEN,&r) == (int *)(NULL))
					 exit(-1);
		  else
		  {
					 for ( c = 0; c < ARRAY_LEN;  c++ )
					 {
								r[c] = c;
								printf("r[%d] = %d\n",c, r[c]);
					 }
					 free(r);
		  }

		  return 0;
}
