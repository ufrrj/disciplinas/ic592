#include <stdio.h>
#include <stdlib.h>

int main(void);

#define MAX_COL	80
#define MAX_ROW	25

int
main(void)
{
	int xi, xf, yi, yf;
	int l, c, t, x, y;
	char border, fill, caracter;
	char	tela[MAX_COL][MAX_ROW];

	printf("Informe o caracter para a borda e para o preencimento\n");
	scanf("%c %c", &border, &fill);
	printf("Digite as coordenadas iniciais XI YI e coordenadas finais XF YF\n");
	printf("Lembrando : (0,0) canto superior esquerdo\n");
	printf("            (80,25) canto inferiro direito\n");
	scanf("%d %d %d %d", &xi, &yi, &xf, &yf);
	if ( xi > xf )
	{
		t = xi;
		xi = xf;
		xf = t;
	}
	if ( yi > yf )
	{
		t = yi;
		yi = yf;
		yf = t;
	}
	x = xf - xi;
	y = yf - yi;
	printf( "X = %d Y = %d border = %c Fill = %c\n", x, y, border, fill);
	for ( c = 0; c < MAX_COL; c++)
		for ( l = 0; l < MAX_ROW; l ++ )
			tela[c][l] = ' ';
	for (c = 0 ; c <= x; c ++)
		for ( l = 0; l <= y; l++)
		{
			if ( c == 0 || c == x  || l == 0 || l == y )
				caracter = border;
			else
				caracter = fill;
			tela[xi+c][yi+l] = caracter;
		}
	for ( l = 0; l < MAX_ROW; l ++ )
	{
		for ( c = 0; c < MAX_COL; c++)
			printf("%c", tela[c][l]);
		puts("");
	}

	return 0;
}
