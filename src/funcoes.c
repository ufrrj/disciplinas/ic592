#include <stdio.h>
#include <stdlib.h>
/*
 * Escopo de variáveis
 * 	Local	-> A "existência" da variável só acontece enquanto o bloco
 * 		onde ela foi criada, existir
 * 	Global	-> A "existẽncia" da variável é válida em todo o código
 *
 * Funções -> Fragmenta e especializa o código
 * Como criar uma função:
 * 	Tipo do retorno Nome(parametros)
 * 	{
 * 	}
 * Tipo:
 * 	[unsigned] [long] int [*]
 * 	float [*]
 * 	double [*]
 *		char [*]
 */
unsigned int counter;

int main(void);
void foo(void);
int bar(int a);
int boo(int a, int b);

void
foo(void)
{
		  int b;
		  b = 1;
		  counter ++;
		  printf("FOO => A = N/A B = %d Counter = %u\n", b, counter);
}

int 
bar(int a)
{
		  int b;
		  b = 2;
		  counter *= a;
		  printf("BAR => A = %d B = %d Counter = %u\n", a, b, counter);
		  return 0;
}

int 
boo(int a, int b)
{
		  printf("BOO => A = %d B = %d Counter = %u\n", a, b, counter);
		  b = 5;
		  a = 6;
		  printf("BOO => A = %d B = %d Counter = %u\n", a, b, counter);
		 return (a < b )? 1: 0;
}

int
main(void)
{
		  int a, b;
		  a = 5;
		  b = 10;
		  counter = 0;
		  printf("MAIN => A = %d B = %d Counter = %u\n", a, b, counter);
		  foo();
		  printf("MAIN => A = %d B = %d Counter = %u\n", a, b, counter);
		  bar(4);
		  printf("MAIN => A = %d B = %d Counter = %u\n", a, b, counter);
		  boo(a,b);
		  printf("MAIN => A = %d B = %d Counter = %u\n", a, b, counter);
		  return 0;
}
