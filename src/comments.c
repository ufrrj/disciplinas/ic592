#include <stdio.h>	// Comentario de uma linha
#include <stdlib.h>	// Comentario em uma linha
							
/*
 	Comentario em bloco: E um comentario onde e necessario utilizar
	mais de uma linha para explicar o que esta acontecendo no codigo.
	Geralmente esses comentarios sao utilizados para explicar o algoritmo
	implementado, ou o porque ter sido implementado da maneira mostrada
*/

/* Prototype da funcao MAIN */
int main(void);

/*
   Chamada da funcao MAIN
   Tipo de retorno: Inteiro (int)
   Parametros: Nenhum
*/
int
main(void)
{
		  printf("Exemplo para demonstrar os dois tipos de comentarios em C\n");
		  printf("// comentario de uma linha\n");
		  printf("/* Comentario em multiplas linhas, ou em bloco */\n");
		  return 0;
}
