#include <stdio.h>
#include <stdlib.h>

int main(void);

int
main(void)
{
		  short int a;
		  char *p;
		  unsigned char *up;
		  int c;

		  a = 0x0001;
		  up = (unsigned char *)(&a);
		  p = &a;

		  printf("Primeiro loop\n");
		  for( c =0; c < sizeof(short int); c++, p++, up++)
		  {
					 printf(" p => [%p] -> %d -> %x -> %u\n", p, *p, *p, *p);
					 printf("up => [%p] -> %d -> %x -> %u\n", up, *up, *up, *up);
		  }

		  a = 0xFF01;
		  up = (unsigned char *)(&a);
		  p = &a;

		  printf("Segundo loop\n");
		  for( c =0; c < sizeof(short int); c++, p++, up++)
		  {
					 printf(" p => [%p] -> %d -> %x -> %u\n", p, *p, *p, *p);
					 printf("up => [%p] -> %d -> %x -> %u\n", up, *up, *up, *up);
		  }
		  return 0;
}
