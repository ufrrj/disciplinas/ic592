/*
 * Esse arquivo demonstra o minimo que um programa em C deve conter
 * #include < ... > permite a inclução de arquivos onde há as definições 
 * das funções, tipos necessários constantes e outras informações para 
 * determinada biblioteca
 * #include < ... >
 * #include " ... "
 * A diferença entre as duas formas do comando include é que na primeira 
 * os arquivos deve estar nos diretórios padrão do compilador, já o segundo 
 * caso o compilador irá buscar no diretório corrente ou nos diretórios 
 * informados pela opção de linha de comando -I 
 */
#include <stdio.h>
#include <stdlib.h>

/*
 * O comando abeixo informa ao compilador que qualquer chamada a função
 * main, deve ser respeitados os tipos de argumentos (void) e de valor de 
 * retorno (int).  Se houver qualquer menção diferente, deve retornar um 
 * erro
 */
int main(void);

/*
 * Corpo da função principal. Em qualquer código escrito em C, a função
 * principal deve retornar um valor para o sistema operacional
 * Valores:
 *  = 0  	indica sucesso, programa executado sem erros
 * <> 0 		indica que houve algum erro, veja o arquivo errno.h, lá tem 
 * 		vários erros que são padrões para a utilização correta das 
 * 		mensagens de erro
 */

int
main(void)
{
		  return 0; // programa executado com sucesso
}
