#include <stdio.h>
#include <stdlib.h>
/*
 * Programa que permite a leitura dos paramentros da linha de comando
 * A função MAIN pode receber as variáveis argc ( total de parametros )
 * e argv ( os parametros ) que informan os parametros fornecidos pelo
 * usuário na linha de comando
 */

int main(int argc, char *argv[]);

int
main(int argc, char *argv[])
{
		  char *buffer;
		  int r;
		  int c;

		  printf("Quantidade de paramentros = %d\n", argc);
		  if ( argc != 2 )
					 printf("Esqueceu de colocar o valor de retorno\n");
		  else
					 r = atoi(argv[1]);  // atoi Ascii to Int
												// transforma uma string em número
		  printf("Valor de Retorno %d\n",r);
		  // Mostra todos os paramentros informados na linha de comando
		  for ( c=0; c < argc; c ++)
					 printf("Argumento %d = %s\n", c, argv[c]);
		  return r;
}
