#include <stdio.h>
#include <stdlib.h>

/*
 * A execução de qualquer operação aritimética é realizada da esquerda 
 * para a direita, obedecendo-se a precedência dos operadores que é
 * a seguinte:
 * 1. ( )			- Parenteses
 * 2. *, / e %		- Multiplicação, divisão e resto da divisão
 * 3. +, -			- Soma e subtração
 */
int main(void);

int
main(void)
{
	int a, b, c;
	a = 4;
	b = 3;
	c = 5;
		
		printf("%d + %d = %d\n", a, b, a + b );
		printf("%d - %d = %d\n", c, b, c - b );
		printf("%d + %d * %d = %d\n", a, b, c, a + b * c);
		printf("( %d + %d ) * %d = %d\n", a, b, c, (a + b) * c);
		printf("%d + %d * %d / %d  = %d\n", a, b, c, a, a + b * c / a);
		printf("%d + %d * %d / %d  = %f\n", a, b, c, a, a + (b * c) * 1.0 / a );
		  return 0; // programa executado com sucesso
}
