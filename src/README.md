# Arquivos exemplos

## Arquivos para usar como exemplos

Use com sabedoria esses arquivos, são exemplos para serem utilizados durante as aulas, utilizando a linguagem C

- io.c                  - Arquivo onde são mostrados os comandos básicos de entrada e saída
- op_aritimeticos.c     - Operadores básicos aritiméticos
- iop_logicas.c         - Operadores lógicos
- ternary.c - Operador ternário
- binary.c
- comments.c
- detec.c
- do.c
- for.c
- getopt_long.c
- helloworld.c
- io.c
- limits_size.c
- limpar.c
- limpar2.c
- maior.c
- media.c
- multiplicacao.c
- multiplicacao2.c
- operadores.c
- pointer2.c
- pointers.c
- return_value.c
- segundograu.c
- sizeof.c          -  Tipos básicos de dados utilizados e seus tamanhos em bytes
- soma.c
- string_size.c
- template.c
- while.c

