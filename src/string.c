#include <stdio.h>
#include <stdlib.h>

int main(void);

int
main(void)
{
		  char *nome1="Fulano";
		  char nome2[]="Fulano";
		  char nome3[10]="Fulano";
		  int a=10;
		  int  *valor1;
		  int  valor2[]={'f', 'u', 'l', 'a', 'n', 'o','\0'};
		  int  valor3[10]={0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

		  valor1 = &a;

		  printf("%c  -  %c  -  %c\n", *nome1, nome2[0], nome3[0]);
		  printf("%c  -  %c  -  %c\n", *(nome1+2), nome2[2], nome3[2]);
		  printf("%s  -  %s  -  %s\n", nome1, nome2, nome3);
		  printf("%s  -  %s  -  %s\n", (nome1+1), (nome2+1), (nome3+1));
		  printf("%d  -  %d  -  %d\n", *valor1, valor2[0], valor3[0]);
		  printf("%d  -  %d  -  %d\n", (valor1+1), (valor2+1), (valor3+1));
		  printf("%d  -  %d  -  %d\n", *(valor1+1), *(valor2+1), *(valor3+1));

		  return 0; // programa executado com sucesso
}
