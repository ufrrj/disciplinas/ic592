#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>

int main(int argc, char *argv[]);
int processArgs(int argc, char* argv[]);

int		fflag, hflag, wflag;
int		fvalue, hvalue, wvalue;
char		*farg, *harg, *warg;

int main(int argc, char *argv[]){
int r;

	r = processArgs(argc, argv);
	printf ("Retorno: %d :\n", r);
	if ( r != 1 ){
			printf("%s [-f filename] [-h [command]] [-w]\n", argv[0]);
			printf("Or use long options\n");
			printf("%s [--file filename] [--help=[command]] [--write]\n", argv[0]);
	}
	return 0;

}

int processArgs(int argc, char* argv[]){
const char* const short_opts = "f:wh:p:x:k:s:";
struct option long_opts[] = {
            {"file"			, optional_argument	, NULL, 'f'},
            {"help"			, optional_argument	, NULL, 'h'},
            {"write"       , 0	, NULL, 'w'},
            {NULL			, 0	, NULL,  0 }
};
int    r;  // -1 - Erro, 0 - empty, 1 - ok
int 	opt;
char  *s = " ";


	opterr = 0;
   r 		 = 0;
	while ((opt = getopt_long(argc, argv, short_opts, long_opts, NULL)) != -1 ){
      switch (opt) {
         case 'f':
				r = 1;
				fflag = 1;
				printf ("File: %s :\n", optarg);
            break;
         case 'w':
				r = 1;
            wflag = 1;
				break;
			case 'h': // -h or --help
				r = 1;
				if ( strlen(optarg) )
					s = optarg;
				printf ("Help: %s :\n", s);
				break;
			case ':':
			case '?':
         default:
            r = -1;
            break;
		}
    }
    return r;
}
