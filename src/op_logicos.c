#include <stdio.h>
#include <stdlib.h>

/*
 * A execução de qualquer operação lógica é realizada da esquerda 
 * para a direita, obedecendo-se a precedência dos operadores que é
 * a seguinte:
 * 1. &		- And bitwise
 * 2. ^		- Xor 
 * 3. |		- Or bitwise
 * 4. &&		- And
 * 5. ||		- Or
 */
int main(void);

int
main(void)
{
	short int a, b, c, d;
	a = 0x01;
	b = 0x05;
	c = 0xF0;
	d = 0x00;
		
		printf(" 0x%02hX  | 0x%02hX = 0x%02hX\n", a, b, a | b );
		printf(" 0x%02hX  & 0x%02hX = 0x%02hX\n", a, b, a & b );
		printf(" 0x%02hX  ^ 0x%02hX = 0x%02hX\n", a, b, a ^ b );
		printf("~0x%02hX  | 0x%02hX = 0x%02hX\n", a, b, ~a | b );
		printf("~0x%02hX  & 0x%02hX = 0x%02hX\n", a, b, ~a & b );
		printf("~0x%02hX  ^ 0x%02hX = 0x%02hX\n", a, b, ~a ^ b );
		printf(" 0x%02hX && 0x%02hX = 0x%02hX\n", c, b, c && b );
		printf(" 0x%02hX || 0x%02hX = 0x%02hX\n", c, b, c || b );
		printf(" 0x%02hX && 0x%02hX = 0x%02hX\n", c, d, c && d );
		printf(" 0x%02hX || 0x%02hX = 0x%02hX\n", c, d, c || d );
		printf(" 0x%02hX || 0x%02hX = 0x%02hX\n", c, d, c || d );
		  return 0; // programa executado com sucesso
		return 0; // programa executado com sucesso
}
