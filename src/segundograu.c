#include <stdio.h>
#include <stdlib.h>
#include <math.h>
/*
 * Exercicio:
 * Faça os algoritimos descritivo e estruturado para calcular as raizes da
 * equação do 2 graus, utilizando baskara
 * Ao final o programa deverá mostrar as raizes ou informar que não é
 * possível calcular as raizes
 *
 * Descritivo:
 * Solicitar ao usuário ps coeficientes da equação
 * Verificar se o coeficiente A é igual a 0, se for, voltar ao passo anterior
 * Calcular o Delta ( Delta=sqrt(b*b - 4*a*c) )
 * Se o delta for >= 0 então
 * 	calcular a primeira raiz ( x1 <- (-b + sqrt(delta))/(2*a)
 * 	Se delta > 0 Então
 * 	calcular a segunda raiz ( x2 <- (-b - sqrt(delta))/(2*a)
 * 	caso contrário segunda raiz será igual a primeira
 * caso contrário
 * 	mostrar "Raizes não reais"
 *
 * Estruturado:
 * 1. Inicio
 * 2. Declarar variáveis
 * 	2.1 a, b, c, x1, x2, delta 
 * 3. Mostrar "Digite os coeficientes da equação"
 * 4. Leia a, b, c
 * 5. Calcular o delta <- b*b - 4*a*c
 * 6. Se delta >= 0 Então
 * 	6.1 x1 <- (-b + sqrt(delta))/(2*a)
 * 	6.2 Se delta > 0 Então
 * 		6.2.1 x2 <- (-b - sqrt(delta))/(2*a)
 * 	6.3 Caso contrário
 * 		6.3.1 x2 <- x1
 * 	6.4 Mostrar "Raizes da equação: ", x1, x2
 * 7. Caso contrário
 * 	7.1 Mostrar "Raizes imaginárias"
 * 8. Fim ( Término )
 *
 */

int main(void);

int
main(void)
{
		  float  a, b, c, x1, x2, delta;
		  puts("Digite os coeficentes da equacao");
		  scanf("%f %f %f", &a, &b, &c);
		  delta = b*b - 4 * a * c;
		  if ( delta >= 0.0 )
		  {
					 x1 = ( -b + sqrt(delta) ) / ( 2 * a );
					 if ( delta > 0 )
								x2 = ( -b - sqrt(delta) ) / ( 2 * a );
					 else
								x2 = x1;
					 printf("Raizes da equacao: %f %f\n", x1, x2);
		  }
		  else
					 printf("Raizes nao reaais\n");

		  return 0;
}
