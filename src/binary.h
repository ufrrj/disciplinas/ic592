#ifndef  __BINARY_H__
#define  __BINARY_H__
#include <stdio.h>
#include <stdlib.h>
/*
**
** Function: binary
** Action:  Transform a unsigned long int value to string (char *)
**     with a binary representative.
**
** Parameters:
**		In:
**			unsigned int v:  Value to converto to string
**			char *buffer:		Buffer with 65 bytes of size to representative
**								binary value
**			char flag:			Flag change how zeros on the left are showing, if
**								flag is true (1) show left zeros, flag is false (0)
**								not show left zeros
**		Out:
**			char *buffer:		String with a binary representative of v
**		Return:
**			char *:				String with a binary representative of v
**
*/

char *binary(unsigned long int v, char *buffer, char flag);

#endif
