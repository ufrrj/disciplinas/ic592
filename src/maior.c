#include <stdio.h>
#include <stdlib.h>
/*
 * Exercicio:
 * Faça os algoritimos descritivo e estruturado para dado 5 valores inteiros 
 * mostrar o maior
 *
 * Descritivo:
 * Solicitar ao usuário 5 valores inteiros
 * Verificar qual deles é o maior
 * Mostrar na tela o resultado
 *
 * Estruturado:
 * 1. Inicio
 * 2. Declarar variáveis
 * 	2.1 a, b, c, d, e, maior
 * 3. Mostrar "Digite cinco valores inteiros"
 * 4. Leia a, b, c, d, e 
 * 5. Se a >= b E a >= c E a >= d E a >= e Então
 * 	5.1 maior <- a
 * 6. Caso contrário
 * 	6.1 Se b >= a E b >= c E b >= d E b >= e Então
 * 		6.1.1 maior <- b
 * 	6.2 Caso contrário
 * 		6.2.1 Se c >= a E c >= b E c >= d E c >= e Então
 * 			6.2.1.1 maior <- c
 * 		6.2.2 Caso contrário
 * 			6.2.2.1 Se d >= a E d >= b E d >= c E d >= e Então
 * 				6.2.2.1.1 maior <- d
 * 			6.2.2.2 Caso COntrário
 * 				6.2.2.2.1 maior <- e
 * 7. Mostrar "O maior valor digitidado = ", maior
 * 8. Fim ( Término )
 * 2 5 1 6 0
 * 2 5 1 1 5
 *
 * 2 > 5 E 2 > 1 E 2 > 6 E 2 > 0 = F
 * 5 > 2 E 5 > 1 E 5 > 6 E 5 > 0 = F
 * 1 > 2 E 1 > 5 E 1 > 6 E 1 > 0 = F
 * 6 > 2 E 6 > 5 E 6 > 1 E 6 > 0 = V
 *
 * 2 >= 5 E 2 >= 1 E 2 >= 1 E 2 >= 5 = F
 * 5 >= 2 E 5 >= 1 E 5 >= 1 E 5 >= 5 = V
 * 1 > 2 E 1 > 5 E 1 > 5 E 1 > 1 = F
 * 1 > 2 E 1 > 5 E 1 > 5 E 1 > 1 = F
 *
 */

int main(void);

int
main(void)
{
		  int a, b, c, d, e, maior;
		  puts("Digite 5 valores inteiros");
		  scanf("%d %d %d %d %d", &a, &b, &c, &d, &e);
		  if ((a >= b) && (a >= c) && (a >= d) && (a >= e))
					 maior = a;
		  else
					 if ((b >= a) && (b >= c) && (b >= d) && (b >= e))
								maior = b;
					 else
								if ((c >= a) && (c >= b) && (c >= d) && (c >= e))
										  maior = c;
								else
										  if ((d >= a) && (d >= b) && (d >= c) && (d >= e))
													 maior = d;
										  else
													 maior = e;
		  printf("O maior valor digitado = %d\n", maior);

		  return 0;
}
