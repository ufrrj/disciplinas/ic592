#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void);


struct registro
	{
			  unsigned int id;
			  unsigned int idade;
			  char nome[100];
	};

int
main(void)
{
		  struct registro r;		// Variável struct registro 
		  struct registro *rp;	// Ponteiro para struct registro 

		  r.id = 1;
		  r.idade = 10;
		  /*
			* char *strcpy( char * restrict dest, const char * restrict source);
			* Forma insegura de atribuir uma string a uma variável 
			* char nome[100], já que a origem pode conter mais de 100
			* caracteres
			*/
		  strcpy(r.nome,"Fulano");
		  /*
			* void * memcpy( void *dest, const void *source, size_t len);
			* Forma segura de atribuição de uma posição de memória para 
			* outra localidade, no caso, de uma const char *source para
			* char *dest ( char *nome[100] ) com o tamanho máximo possível
			* para a cópia
			*/
			memcpy(r.nome, "fulano", 100);
			rp = &r;
			printf("Usando a variavel r\nId=%u\nIdade=%u\nNome=%s\n", 
								 r.id, r.idade, r.nome);
			printf("Usando o ponteiro rp\nId=%u\nIdade=%u\nNome=%s\n", 
								 rp->id, rp->idade, rp->nome);
			return 0;
}	  
