#include <stdio.h>
#include <stdlib.h>

int main(void);

int
main (void)
{

	char nome[]="Fulano de Tal da Silva\0";

	int contador = 0;
	int branco=0;
	int vogais=0;

	while ( nome[contador] != '\0' )
	{
		if ( nome[contador] == ' ' )
			branco ++;
		if ( 
				nome[contador] == 97  || nome[contador] == 65  ||
				nome[contador] == 'e' || nome[contador] == 'E'  ||
				nome[contador] == 'i' || nome[contador] == 'I'  ||
				nome[contador] == 'o' || nome[contador] == 'O'  ||
				nome[contador] == 'u' || nome[contador] == 'U'
			)
			vogais++;
		contador ++;
	}

	printf("Tamanho da string %d\n",contador);
	printf("Tamanho da string %lu\n",sizeof(nome));
	printf("Tamanho da string %lu\n",sizeof(nome[2]));
	printf("Espacos em branco %d\n",branco);
	printf("Vogais %d\n", vogais);

	return 0;
}
