#include <stdio.h>
#include <stdlib.h>

int main(void);

int
main(void)
{
		  int a,c;
		  int *p;

		  a = 10;
		  p = (int *)(malloc(sizeof(int) * 10)); 
		  printf("%d - %X\n", *p, *p );
		  *p = a;
		  *(p+2) = -7;
		  *(p+5) = 30;
		  for ( c = 0; c < 10; c++)
			  printf("Posicao: %d  Conteudo: %d - %X\n", c, *(p + c), *(p + c) );
		  free(p);

		  return 0;
}
