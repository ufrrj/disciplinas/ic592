#include <stdio.h>
#include <stdlib.h>
#include <binary.h>
/*
**
** Function: binary
** Action:  Transform a unsigned long int value to string (char *)
**     with a binary representative.
**
** Parameters:
**		In:
**			unsigned int v:  Value to convert to string
**			char *buffer:		Buffer with 65 bytes of size to representative
**								binary value
**			char flag:			Flag change how zeros on the left are showing, if
**								flag is true (1) show left zeros, flag is false (0)
**								not show left zeros
**		Out:
**			char *buffer:		String with a binary representative of v
**		Return:
**			char *:				String with a binary representative of v
**
*/
char *binary(unsigned long int v, char *buffer, char flag){
unsigned int size;
unsigned long int mask, a;
unsigned int c,i,k;

	size = sizeof(v) * 8;  // Bytes * bits
	for(int i=0; i < 64; i++)	// fill buffer with 0's
		buffer[i] = '0';
	buffer[size] = '\0';		// set last space with NULL ('\0')
	mask = 0x00000001;		// set mask to 1
	a = v;
	c = 0;
	while (a){					// Fill buffer with 0's or 1's
		a = a & mask;			// from end to begin
		buffer[size - 1 - c] = '0' + (char)(a);
		c++;
		a = v >> c;
	}
	c--;
	i=0;
	k=c;
	if ( (c < 64) && flag ){		// If flag is true and the size is lees than
		while( i < c ){				// 64 bits, fill the buffer from begin to end
			buffer[i] = buffer[size-1-k];
			i++;
			k--;
		}
		buffer[c+1] = '\0';			// Set new end string pos
	}
	return buffer;
}
