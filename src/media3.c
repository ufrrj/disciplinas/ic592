#include <stdio.h>
#include <stdlib.h>

int main(void);

int
main(void)
{
		  int soma, valor, contador;
		  float media;

		  soma = 0;
		  contador = 0;
		  do
		  {
					 printf("Digite um valor: ( <= 0  para parar)\n");
					 scanf("%d",&valor);
					 if ( valor > 0 )
					 {
								soma = soma + valor;
		  						contador = contador + 1;
					 }
		  }
		  while ( valor > 0 );
		  media =  (soma * 1.0) / contador;
		  printf("Media = %f\n", media);
		  return 0;
}
