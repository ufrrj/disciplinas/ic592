#include <stdio.h>
#include <stdlib.h>

#include <limits.h>


int main(void);

int
main(void)
{
	int vi, vf, passo;
	int contador;
	short	int si;
	unsigned short	int usi;
	int parar;

	printf("Tamanho em bytes:\n\
CHAR          %d\n\
SHORT INT     %d\n\
INT           %d\n\
LONG INT      %d\n\
LONG LONG INT %d\n\
FLOAT         %d\n\
DOUBLE        %d\n\
" , sizeof(char),sizeof(short int), sizeof(int)
  , sizeof(long int),sizeof(long long int)
  , sizeof(float), sizeof(double));

	printf ("Limites na plataforma 64 bits:\n\
CHAR ..................... %d\n\
SIGNED CHAR MIN .......... %d\n\
            MAX .......... %d\n\
UNSIGNED CHAR MAX ........ %u\n\
CHAR MIN ................. %d\n\
     MAX ................. %d\n\
SHORT INT MIN ............ %d\n\
          MAX ............ %d\n\
UNSIOGNED SHOT INT MAX ... %u\n\
INT MIN .................. %d\n\
    MAX .................. %d\n\
UNSIGNED INT MAX ......... %u\n\
LONG INT MIN ............. %ld\n\
         MAX ............. %ld\n\
UNSIGNED LONG INT MAX .... %lu\n\
LONG LONG INT MIN ........ %lld\n\
              MAX ........ %lld\n\
UNSIGNED LONG LONG INT ... %llu\n\
",
CHAR_BIT, SCHAR_MIN, SCHAR_MAX, UCHAR_MAX, CHAR_MIN, CHAR_MAX,
SHRT_MIN, SHRT_MAX, USHRT_MAX,
INT_MIN, INT_MAX, UINT_MAX,
LONG_MIN, LONG_MAX, ULONG_MAX,
LLONG_MIN, LLONG_MAX, ULLONG_MAX);

	return 0;
}
