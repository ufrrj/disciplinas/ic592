#include <stdio.h>
#include <stdlib.h>
/*
 * Operadores lógicos bitwise
 * &	- And
 * |	- Or
 * ^	- Xor
 * ~	- Not
 *
 * Operadores Lógicos
 * &&	- And
 * ||	- Or
 * ^	- Xor
 * !	- Not
 *
 * Operador de deslocamento
 * <<	- Deslocamento à esquerda
 * >>	- Deslocamento à direita
 *
 * Operadores aritméticos
 * +	- Soma
 * -	- Subtração
 * *	- Multiplação
 * /	- Divisão
 * %	- Resto da divisão
 * ( ) - Altera o presedência das operações
 *
 * Primeiro *, /
 * Segundo  +, -
 *
 * 10 * 2 / 5 = 4
 */

int main(void); // Typedef da função main

int
main(void)
{
		  int a,b,c;

		  a = 0xFFFFFF; // binário: 1111 1111 1111 1111 1111 1111
		  b = 0xAA;		 // binário  0000 0000 0000 0000 1010 1010
		  c = 0x55;		 // binário  0000 0000 0000 0000 0101 0101
		  printf("A & B %08X %08x\n", a & b, a & b );
		  printf("A | B %08X %08x\n", a | b, a | b );
		  printf("A ^ B %08X %08x\n", a ^ b, a ^ b );
		  printf("   ~A %08X %08x\n", ~a, ~a );

		  printf("Tabela Verdade And\n");
		  printf("A B | S\n");
		  printf("%1d %1d | %1d\n",0,0, 0 && 0);
		  printf("%1d %1d | %1d\n",0,1, 0 && 1);
		  printf("%1d %1d | %1d\n",1,0, 1 && 0);
		  printf("%1d %1d | %1d\n",1,1, 1 && 1);

		  printf("Tabela Verdade Or\n");
		  printf("A B | S\n");
		  printf("%1d %1d | %1d\n",0,0, 0 || 0);
		  printf("%1d %1d | %1d\n",0,1, 0 || 1);
		  printf("%1d %1d | %1d\n",1,0, 1 || 0);
		  printf("%1d %1d | %1d\n",1,1, 1 || 1);

		  printf("Tabela Verdade Xor\n");
		  printf("A B | S\n");
		  printf("%1d %1d | %1d\n",0,0, 0 ^ 0);
		  printf("%1d %1d | %1d\n",0,1, 0 ^ 1);
		  printf("%1d %1d | %1d\n",1,0, 1 ^ 0);
		  printf("%1d %1d | %1d\n",1,1, 1 ^ 1);
		 
		  printf("Tabela Verdade Not\n");
		  printf("A | S\n");
		  printf("%1d | %1d\n",0, !0);
		  printf("%1d | %1d\n",1, !1);

		  printf("( a > b ) = %d\n( a == c)) = %d\n", (a > b), (a == c));
		  if (( a > b ) && ( a == c ))
		  {
					 printf("(( a > b ) && ( a == c)) = Verdadeiro\n");
		  }
		  else
		  {
					 printf("(( a > b ) && ( a == c)) = Falso\n");
		  }
		  printf( "%X %X\n", b, b << 1);
		  printf("0xAA -> 1010 1010 << 1 -> 1 0101 0100\n");
		  // 0xAA -> 1010 1010 << 1 -> 1 0101 0100
		  printf( "%X %X\n", b, b >> 1);
		  printf("0xAA -> 1010 1010 >> 1 -> 0101 0101\n");
		  // 0xAA -> 1010 1010 >> 1 -> 1 0101 0101
		  printf( "%X %X\n", b, b >> 2);
		  printf("0xAA -> 1010 1010 >> 2 -> 0010 1010\n");
		  // 0xAA -> 1010 1010 >> 2 -> 0010 1010 
		  printf("%08X\n",a);
		  a = a >> 2;
		  printf("%08X\n",a);
		  a = a << 2;
		  printf("%08X\n",a);

		  printf("%f\n", (15 * 3.0 /  5 + 1  ));
		  printf("%f\n", (15 * 3.0 / (5 + 1) ));
		  printf("%f\n", (  1 + 15   * 3.0 /  5 + 1  ));
		  printf("%f\n", (( 1 + 15 ) * 3.0 / (5 + 1) ));
		  printf("%f\n", (  1 + 15   / 3.0 *  5 + 1  ));
		  printf("%f\n", (( 1 + 15 ) / 3.0 * (5 + 1) ));
		  printf("%d %d\n", 10 % 2, 9 % 2);
		  return 0;
}
