#include <stdio.h>
#include <stdlib.h>
#include <string.h>  // Typedef da funcao size_t sizeof(const char *)
							// strlen retorna o tamanho da string, 
							// sem computar o caracter de fim de string '\0'
int main(void);

int
main(void)
{

		  short int a,b,c,d;
		  short int *p;
		  short int *q;
		  char *cp = "Nome de fulano";

		  a = 10;
		  b = 5;
		  c = -1;
		  d = 'A';
		  p = &a;
		  printf("Endereco CP[%p] string[%s] comprimento[%lu]\n", cp  , cp  , strlen(cp));
		  printf("Endereco CP[%p] string[%s] comprimento[%lu]\n", cp+1, cp+1, strlen(cp+1));
		  printf("Endereco  P[%p] Ponteiro[%d] Variavel[%d]\n", p  , *p, a);
		  printf("Endereco  P[%p] Ponteiro[%d] Variavel[%d]\n", p+1, *(p+1), a);
		  *p = 30;
		  printf("Endereco  P[%p] Ponteiro[%d] Variavel[%d]\n", p, *p, a);

		  printf("&A[%p] &B[%p] &C[%p] &D[%p]  P[%p]  (P+1)[%p]\n", &a, &b, &c, &d, p, (p+1));
		  printf(" A[%11d]  B[%11d]  C[%11d]  D[%11d] *P[%11d] *(P+1)[%11d]\n", a, b, c, d, *p, *(p+1));
		  printf("&A[%p] &B[%p] &C[%p] &D[%p]  P[%p]  (P-1)[%p]\n", &a, &b, &c, &d, p, (p-1));
		  printf(" A[%11d]  B[%11d]  C[%11d]  D[%11d] *P[%11d] *(P-1)[%11d]\n", a, b, c, d, *p, *(p-1));
		  printf("&A[%p] &B[%p] &C[%p] &D[%p]  P[%p]  (P-1)[%p]\n", &a, &b, &c, &d, p, (p-2));
		  printf(" A[%11d]  B[%11d]  C[%11d]  D[%11d] *P[%11d] *(P-1)[%11d]\n", a, b, c, d, *p, *(p-2));
		  printf("&A[%p] &B[%p] &C[%p] &D[%p]  P[%p]  (P-1)[%p]\n", &a, &b, &c, &d, p, (p-3));
		  printf(" A[%11d]  B[%11d]  C[%11d]  D[%11d] *P[%11d] *(P-1)[%11d]\n", a, b, c, d, *p, *(p-3));
		  return 0;
		  /*
			*  8G memoria = 10 0000 0000 0000 0000 0000 0000 0000 0000
			*               2  0    0    0    0    0    0    0    0
			*               0x0200000000
			* 16G memoria = 0x0400000000
			* 32G memoria = 0x0800000000
			* 64G memoria = 0x1000000000
			*/                                     
}
