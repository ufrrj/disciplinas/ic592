#include <stdio.h>
#include <stdlib.h>

unsigned int counter;

unsigned long int fatorial(int a);
int main(void);

unsigned long int
fatorial(int a)
{
		  /* Fatorial:
			* 5! => 5 * 4 * 3 * 2 * 1
			*/
		  unsigned long int f;
		  printf("Counter = %u A = %d\n", counter, a);
		  counter ++;
		  if ( a <= 1 )
					 f = 1;
		  else
		  {
					 f = a * fatorial(a - 1);
					 printf("Counter = %u f = %lu\n", --counter, f);
		  }

		  return f;
}

int
main(void)
{
		  int a;
		  counter = 0;
		  printf("Digite o valor: \n");
		  scanf("%d", &a);
		  printf("%d! = %lu\n", a, fatorial(a));
		  return 0;
}
