#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int main(void);

int
main(void)
{
	char valor[65]; // 1 caracter por bit + 1 para o '\0'
	char c[2];
	int counter, tamanho;
	unsigned int decimal;

	decimal = 0;
	c[1]='\0';
	printf("Tamanho = %lu\n", strlen(valor));
	for ( counter = 0; counter < 65; counter ++ )
		valor[counter] = '\0';
	printf("Digite um valor binario com no maximo 64bits\n");
	scanf("%s",valor);
	printf("Tamanho = %lu\n", strlen(valor));
	counter = 0;

	while ( valor[counter] != '\0' )
		counter ++;
	tamanho = counter;
	for ( ; counter ; counter -- )
	{
		c[0] = valor[tamanho -counter];
		decimal = decimal + atoi(c) * pow(2, counter-1);
	}
	printf("Binario = %s decimal %d \n", valor, decimal );


	return 0;
}

