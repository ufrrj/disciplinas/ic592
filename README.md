# Disciplina IC592

## Estrutura de diretórios

```shell
.
├── README.md
├── docs
├── listas
└── src

```

O arquivo README.md é o arquivo onde reside as informações sobre o repositório. Estas informações que você está lendo estão contidas neste arquivo.
O diretório _docs_ contém arquivos de aula, em pdf, e são uma ótima fonte de consulta. Caso utilize total ou parcialmente as informações dele para complementar um outro material, basta incluir a referência à minha autoria.
O diretório _listas_ contém listas de exercícios para cada parte do curso de programação, as respostas serão dadas em sala, mas se seguir o curso, você mesmo terá capacidade de solucioná-los.
O diretório _src_ é onde reside os arquivos dos códigos fonte dos exemplos utilizados nas anotações de aula.

## Sobre o autor

Sou professor da [Universidade Federal Rural do Rio de Janeiro](https://www.ufrrj.br), onde leciono para o curso de [Sistemas de Informação](https://cursos.ufrrj.br/grad/sistemas), várias disciplinas, a saber:

- IC592 - Linguagem de programação I
- IC501 - Computação I
- IC505 - Redes de computadores
- IC593 - Introdução a sistemas web
- IC596 - Linguagem de programação II
- IC599 - Sistemas web I
- IC502 - Computação II
- IC806 - Sistemas operacionais
- IC832 - Sistemas operacionais Linux
- IC834 - Introdução à programação de jogos
- IC836 - Introdução a computação gráfica
- IC841 - Tópicos Especiais em redes de computadores
- IC846 - Tópicos especiais em desenvolvimento web

As minhas linhas de pesquisa são:

- Visão computacional
- Robotica
- Segurança
- Software Livre

Acesse a minha página pessoal para maiores informações [Página pessoal](https://www.rizzo.eng.br)
